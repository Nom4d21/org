---
title: 'Week of 10/04/2021'
description: Catch up with the latest updates from Grey Software!
category: 'Weekly Updates'
position: 3002
---

## Overview

This week we completed our 4.1 milestone and made preparations to welcome the open source community back into our
Discord community for Hacktoberfest.

We also published our [`/founding-story`](https://grey.software/founding-story), and received over 180 unique visits!

<cta-button text="v4.1 Milestone" link="https://gitlab.com/groups/grey-software/-/milestones/1"></cta-button>
<cta-button text="Founding Story" link="https://grey.software/founding-story"></cta-button>

> **Reminder of our major goals from last week**
>
> 1. Update our pitch as feedback comes in
> 2. Reopen our Discord community to work with hacktoberfest contributors

### What we did this past week

1. Published a new iteration of [`/pitch`](https://grey.software/pitch) with updated info about our revenue channels and
   go to market strategy

![Strategic Map](https://grey.software/pitch/28.png)

2. Opened our [Discord community server](http://community.grey.software) to the public for
   [`/hacktoberfest`](https://grey.software/hacktoberfest)

   ![](https://i.imgur.com/R05VKko.png)

3. We got great feedback from Pioneer expert but unfortunately it was for an outdated project description.

> If I understand correctly, you're building an organization that creates open-source software which students can
> contribute to, building software that benefits the world and educating students simultaneously. This is a great
> mission—however, it might be difficult to sustain purely through donations. I noticed in your latest update you're
> looking to raise non-dilutive capital. Have you thought about other revenue streams, i.e., charging companies that
> hire from the students that contribute to the open source projects?

### Our goals for Next Week

1. Refine and published a new iteration of our [`/pitch`](https://grey.software/pitch) and Pioneer project description

2. Write emails to Social Capital, Underscore VC, OSS Capital, and others

3. Reach out and strategize about our strategy for hosting university workshops at PAK-UETM, PAK-LUMS, CAN-UTM

## Team Updates

### Arsala

Since I usually write the overall updates, I don't usually duplicate my updates in the team section so others can
highlight their contributions.

This week, however, I'd like to share how gateful I am because the encouragement and support I received after publishing
the [`/founding-story`](https://grey.software/founding-story) was than I could've imagined.

### Raj

I'm taking a short break from my regular design work at Grey software to do a deep dive into branding, and I'm excited
to check back in with everything I've learned!

### Faraz

This week I continued to work on the [automation dashboard](https://gitlab.com/groups/grey-software/-/epics/5) to
automate the time-consuming manual tasks currently being performed by Grey software team members.

- [`spacing-config`](https://https://gitlab.com/grey-software/automation/-/issues/3) generates custom tailwind spacing
  configuration with a click of a button. Output is displayed in the text area which can be edited, copy to clipboard or
  save in a file.

- [`aggregate-analytics` ](https://gitlab.com/grey-software/automation/-/issues/4) renders the plausible analytics for a
  period selected from the dropdown. Generated Analytics can be saved in a JSON file or copy to a clipboard.

- [`Sponsors`](https://gitlab.com/grey-software/automation/-/issues/2) parse the uploaded GitHub sponsor report to the
  format to be used across the ecosystem’s websites. Generated data is displayed to UI with Stats.

### Laiba

This week I started working on workshop ideas in the light of
[collaborating with UET Mardan](https://https://gitlab.com/grey-software/universities/pakistan-uet-mardan/org/-/issues/4).

I took
[feedback from their students](https://https://https://gitlab.com/grey-software/universities/pakistan-uet-mardan/org/-/issues/9)
so we can plan better workshop's content for them.

### Zakir

This week my MRs on
the[`grey-docs`](https://gitlab.com/grey-software/templates/grey-docs/-/merge_requests?scope=all&state=opened&assignee_username=ZakirBangash)and[`website`](https://gitlab.com/grey-software/website/-/merge_requests?scope=all&state=opened&assignee_username=ZakirBangash)repos
from last week were successfully merged. I also
[refactored the Navigation dropdown links on the website](https://gitlab.com/grey-software/website/-/merge_requests/99).
