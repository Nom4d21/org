---
title: 'Week of 09/13/2021'
description: Catch up with the latest updates from Grey Software!
category: 'Weekly Updates'
position: 3005
---

## Overview

This week, our team closed 10 issues worth 56 points, merged 30 requests, and made 320 commits.

<cta-button text="Sprint Team Board" link="https://gitlab.com/groups/grey-software/-/boards/3080709?scope=all&iteration_title=v4.1%20Sprint%202"></cta-button>

### What we did this past week

1: We revised our pitch strategy and concluded that we needed different pitches for different audiences. Angel investors
were the highest priority audience.

2: We published Focused Browsing and promoted the product on social media.

### Our goals for Next Week

1: Update the investor pitch as feedback comes.

2: Follow up with UET Mardan about Final Year Project collaboration.

3: Promote Grey Software and Focused Browsing more actively on Hackernews, Product Hunt, Reddit, etc.

## Team Updates

### Arsala

This week I was primarily responsible for the Grey software pitch, through which I learned many new things about
presentation design and content writing!

I also fixed Focused Browsing up for a new release, which has been published to both of the Chrome and Firefox stores.

This week was busy on the web ecosystem side as well, with Faraz, Laiba, and Zakir adding many new features alongside
me.

### Faraz

This week I mainly worked on tasks that had to do with the landing website's
[credits page](https://grey.software/credits).

I implemented the updated DonorCard designs and worked on a script that parses sponsorship reports form Github into a
data structure that could be used by our components.

#### Issues

| Issues                                                                                                                                    | Progress                  | Status             |
| ----------------------------------------------------------------------------------------------------------------------------------------- | ------------------------- | ------------------ |
| [#82](https://gitlab.com/grey-software/website/-/issues/82) We need to design and engineer the Donor Card Component                       | Completed                 | Closed             |
| [#103](https://gitlab.com/grey-software/org/-/issues/103) We need to write a program that parses github sponsor data for our credits page | Completed                 | Closed             |
| [#71](https://gitlab.com/grey-software/website/-/issues/71) We need to revamp the credits page                                            | First two tasks completed | Waiting for Design |
| [#83](https://gitlab.com/grey-software/website/-/issues/83) We need to design and engineer the Sponsor Card Component                     | ---                       | Waiting for design |

### Laiba Gul

This week I was responsible for submitting our weekly pioneer update and adding ~20 pages to the resources.grey.software
website.

#### Merge Requests

| Merge Requests                                                                                                                                      | Progress  | Status |
| --------------------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------ |
| [!20](https://gitlab.com/grey-software/resources/-/merge_requests/20) update: new pages added                                                       | Completed | Review |
| [!19](https://gitlab.com/grey-software/resources/-/merge_requests/19) update: youtube videos now responsive on different devices.                   | Completed | Closed |
| [!16](https://gitlab.com/grey-software/resources/-/merge_requests/16) Update: artificial intelligence category added                                | Completed | Closed |
| [!13](https://gitlab.com/grey-software/resources/-/merge_requests/13) update : page added to web development category.                              | Completed | Closed |
| [!12](https://gitlab.com/grey-software/resources/-/merge_requests/12) update : cs concepts and database page added                                  | Completed | Closed |
| [!11](https://gitlab.com/grey-software/resources/-/merge_requests/11) update: javascript libraries,framework,methods and runtime environment added. | Completed | Closed |
| [!10](https://gitlab.com/grey-software/resources/-/merge_requests/10) update: css videos added                                                      | Completed | Closed |

### Zakir Bangash

This week I worked on the support page and the ecosystem partnership section for the landing website. Additionally, I
worked on a script that will help grey software designers and engineers follow consistent spacing guidelines.

#### Issues

| Issues                                                                                                                                 | Progress  | Status       |
| -------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------------ |
| [#79](https://gitlab.com/grey-software/website/-/issues/79) We need to engineer the corporate sponsorship section.                     | Completed | Review stage |
| [#75](https://gitlab.com/grey-software/website/-/issues/75) We need to have a support page for landing website.                        | Completed | Review stage |
| [#105](https://gitlab.com/grey-software/org/-/issues/105) We need a script that can generate a Tailwind spacing scale for our websites | Completed | Closed       |
