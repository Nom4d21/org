---
title: 'Week of 09/27/2021'
description: Catch up with the latest updates from Grey Software!
category: 'Weekly Updates'
position: 3003
---

## Overview

<cta-button text="Sprint Team Board" link="https://gitlab.com/groups/grey-software/-/boards/3080709?scope=all&iteration_title=Week%20of%20Sep-20th%20-%20v4.1%20Sprint%203"></cta-button>

> **Reminder of our major goals from last week**
>
> 1. Publish a polished final draft of our pitch to raise non-equity-dilutive VC capital.
> 2. Prepare Grey Software for Hacktoberfest

### What we did this past week

1. Published new /pitch

2. Prepared /hacktoberfest

3. Updated branding copy on website Hero and social profiles

4. Added new pages and responsive polish to our websites

### Our goals for Next Week

1. Update our pitch as feedback comes in

2. Reopen our Discord community to work with hacktoberfest contributors

3. Enjoy building open source software with people around the world

## Team Updates

### Faraz

We decided to turn our automation repository into a admin dashboard website. So this week I worked on building dashboard
and integrated two scripts,`spacing-config` and `aggregate-analytics`.

- `spacing-config` will generate custom tailwind spacing configuration with a click of button. Output will be displayed
  in text area which can be edited, copy to clipboard or save in a file. ![](https://i.imgur.com/1SETVGV.png)

- `aggregate-analytics` will render the plausible analytics for a time period selected from dropdown.Generated Analytics
  can be save in a JSON file or copy to clipboard![](https://i.imgur.com/arexSsi.png)

### Zakir

This week I worked on updating the Navbar and Footer Components across the Grey Software web ecosystem!

I opened MRs on the
[grey-docs](https://gitlab.com/grey-software/templates/grey-docs/-/merge_requests?scope=all&state=merged&assignee_username=ZakirBangash)
and [website](https://gitlab.com/grey-software/website/-/merge_requests/92) repos, and hope to see my work merged in by
next week.
